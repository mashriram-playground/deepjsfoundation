function object_is(val1, val2) {
  signOfval1 = Math.sign(val1);
  signOfval2 = Math.sign(val2);
  if (Number.isNaN(val1) && Number.isNaN(val2)) {
    return true;
  } else if (signOfval1 === 0 && signOfval2 === 0) {
    if (-Infinity === 1 / signOfval1 && -Infinity === 1 / signOfval2) {
      return true;
    } else if (Infinity === 1 / signOfval1 && Infinity === 1 / signOfval2) {
      return true;
    } else {
      return false;
    }
  } else if (val1 === val2) {
    return true;
  } else {
    return false;
  }
}
