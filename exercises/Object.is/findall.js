function findAll(value, array) {
  let matchArray = [];
  for (const item of array) {
    if (isBothBoolean(value, item)) {
      if (value == item) {
        matchArray.push(item);
      }
    } else if (
      isNeitherBoolean(value, item) &&
      isNeitherEmptyString(value, item) &&
      isNeitherNaNOrInfinity(value, item)
    ) {
      if (numberComparison(value, item)) {
        matchArray.push(item);
      } else if (isNullOrUndefined(value, item)) {
        if (value == item) {
          matchArray.push(item);
        }
      } else if (Object.is(value, item)) {
        matchArray.push(item);
      }
    } else if (!isNeitherNaNOrInfinity(value, item)) {
      if (value === item) {
        matchArray.push(item);
      } else if (Number.isNaN(value) && Number.isNaN(item)) {
        matchArray.push(item);
      }
    } else if (!isNeitherEmptyString(value, item)) {
      if (value === item) {
        matchArray.push(item);
      } else if (typeof value == "object" || typeof item == "object") {
        if (value === item) {
          matchArray.push(item);
        }
      }
    }
  }
  return matchArray;
}

function isNeitherBoolean(value1, value2) {
  return !(typeof value1 == "boolean" || typeof value2 == "boolean");
}
function isNeitherEmptyString(value1, value2) {
  return !(String(value1).trim() == "" || String(value2).trim() == "");
}
function isNeitherNaNOrInfinity(value1, value2) {
  if (Number.isNaN(value1) || Number.isNaN(value2)) {
    return false;
  } else if (value1 == Infinity || value2 == -Infinity) {
    return false;
  } else {
    return true;
  }
}
function isBothBoolean(value1, value2) {
  return typeof value1 == "boolean" && typeof value2 == "boolean";
}

function isNullOrUndefined(value1, value2) {
  if (value1 == null || value2 == null) {
    return true;
  } else {
    return false;
  }
}
function numberComparison(value1, value2) {
  if (Object.is(-0, value1) || Object.is(-0, value2)) {
    return Object.is(value1, value2);
  } else if (
    (typeof value2 == "number" && typeof value1 == "string") ||
    (typeof value1 == "number" && typeof value2 == "string")
  ) {
    return Number(value1) == Number(value2);
  } else if (typeof value1 == "number" && typeof value2 == "number") {
    return value1 == value2;
  } else {
    return false;
  }
}

var myObj = { a: 2 };

var values = [
  null,
  undefined,
  -0,
  0,
  13,
  42,
  NaN,
  -Infinity,
  Infinity,
  "",
  "0",
  "42",
  "42hello",
  "true",
  "NaN",
  true,
  false,
  myObj,
];

// let x = findAll(NaN, values);
// console.log(x);
console.log("-------------------------------");
console.log(setsMatch(findAll(null, values), [null, undefined]) === true);
console.log(setsMatch(findAll(undefined, values), [null, undefined]) === true);
console.log(setsMatch(findAll(0, values), [0, "0"]) === true);
console.log(setsMatch(findAll(-0, values), [-0]) === true);
console.log(setsMatch(findAll(13, values), [13]) === true);
console.log(setsMatch(findAll(42, values), [42, "42"]) === true);
console.log(setsMatch(findAll(NaN, values), [NaN]) === true);
console.log(setsMatch(findAll(-Infinity, values), [-Infinity]) === true);
console.log(setsMatch(findAll(Infinity, values), [Infinity]) === true);
console.log(setsMatch(findAll("", values), [""]) === true);
console.log(setsMatch(findAll("0", values), [0, "0"]) === true);
console.log(setsMatch(findAll("42", values), [42, "42"]) === true);
console.log(setsMatch(findAll("42hello", values), ["42hello"]) === true);
console.log(setsMatch(findAll("true", values), ["true"]) === true);
console.log(setsMatch(findAll(true, values), [true]) === true);
console.log(setsMatch(findAll(false, values), [false]) === true);
console.log(setsMatch(findAll(myObj, values), [myObj]) === true);

function setsMatch(arr1, arr2) {
  if (
    Array.isArray(arr1) &&
    Array.isArray(arr2) &&
    arr1.length == arr2.length
  ) {
    for (let v of arr1) {
      if (!arr2.includes(v)) return false;
    }
    return true;
  }
  return false;
}
