let myAge = Number("0o46"); // 38
console.log("Number('0o46')     => ", myAge);

let myNextAge = Number("39"); // 39
console.log("Number('39')       => ", myNextAge);

let myCatAge = Number("n/a"); // NaN
console.log("Number('n/a')      => ", myCatAge);

let diff = myAge - "my son's age"; // NaN
console.log("diffBetNumberAndNaN => ", diff);

console.log("NaN == NaN         => ", myCatAge == myCatAge); // false

//NaN is not equal to itself, this is according to IEEE spec
//NaN is the only value that is not equal to itself
console.log("NaN === NaN        => ", myCatAge === myCatAge); // false

//Any operation done on a NaN would result in NaN
console.log("NaN + NaN          => ", NaN + NaN); // NaN

console.log("NaN - NaN          => ", NaN - NaN); // NaN

console.log("NaN * NaN          => ", NaN * NaN); // NaN

console.log("NaN / NaN          => ", NaN / NaN); // NaN

console.log("isNaN(38)       => ", isNaN(myAge)); // false

console.log("isNaN(NaN)    => ", isNaN(myCatAge)); // true

//the below statement returns true even though what was passed is a string
//as isNaN coerces what is passed to it to a Number before checking if it is NaN
//So, the string when coerced to a Number results in NaN and hence isNaN returns true

console.log("isNaN(my son's age) => ", isNaN("my son's age")); // true

console.log("Number.isNaN(myCatAge) => ", Number.isNaN(myCatAge)); // true

console.log("Number.isNaN('my sons age') => ", Number.isNaN("my son's age")); // false

//when you do a numeric operation and get a value, the type of that value is expected to be a Number
//So type of NaN is a Number but a Invalid Number

//Use NaN instead of the below when you want to return a number but there's no valid number
//e.g indexOf should have returned NaN instead of -1 when the element is not present
//So use NaN to indicate a invalid number when doing a numeric operation, instead of the below
// undefined - it is of type undefined and not number
// null     - it is of type object and not a number
// false    - it is of type bool
// -1       - it is a number but it is just incorrect to represent that there is no valid number
// 0        - it is a number but once again it is not a right way to represent that there is no valid number
