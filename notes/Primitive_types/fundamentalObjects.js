//Create an object using new operator for the following
// - new Object()
// - new Date()
// - new Array()
// - new Function()
// - new RegExp()
// - new Error()

// Don't use new operator for the following but use them as functions not constructors
// - String()
// - Number()
// - Boolean()