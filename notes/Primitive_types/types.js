console.log(typeof v); //undefined

var v;
console.log(typeof v); //undefined

v = "";
console.log(typeof v); // string

v = 12;
console.log(typeof v); // number

v = true;
console.log(typeof v); // boolean

v = Symbol();
console.log(typeof v); // symbol

v = 42n;
console.log(typeof v); // bigint

v = BigInt(42);
console.log(typeof v); // bigint

v = null;
console.log(typeof null); // object -- note that it is not null

v = {};
console.log(typeof v); // object

v = [];
console.log(typeof v); // object -- note that it is not array

v = function name(params) {};
console.log(typeof v); // function

console.log(typeof NaN); // Number

console.log(typeof undefined); // undefined

//typeof operator always returns a string
//typeof v === undefined would never be true even if the value of v is undefined
//you should check typeof v === 'undefined'

//if typeof returns "object" then it could either be a value null or an object or an array
//since it can be null, you need to check if it is not null before trying to parse the object

//unset a primitive variable, use undefined
//unset a object type, use null
//typeof null should have returned "null" but it returns "object" which is a bug

//typeof returns undefined in both cases
// 1. if a variable that is not even declared is provided
// 2. if a variable is declared and initialized but not given a value

//undefined vs undeclared vs uninitialized(aka TDZ)
// undeclared - denotes that a variable is not even declared
// uninitialized - denotes that a variable has been declared but not initialized
// undefined -  denotes that a variable is declared and initialized but a value is not assinged

//typeof is the only operator that can access a variable that has never been declared and not throw an error

//variables in block scope don't get initialized (not set to undefined) it is off-limits
//accessing in that time would throw an error (TDZ error)
