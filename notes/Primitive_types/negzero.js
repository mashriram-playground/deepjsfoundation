var negzero = -0;
console.log(negzero); // -0

console.log("negzero === -0 => ", negzero === -0); // true

console.log(negzero.toString()); // 0 -- OOPS

console.log("negzero ===  0 => ", negzero === 0); // true  --- OOPS

console.log("negzero <  0 => ", negzero < 0); // false
console.log("negzero >  0 => ", negzero > 0); // false

console.log("negzero <  -0 => ", negzero < -0); // false
console.log("negzero >  -0 => ", negzero > -0); // false

console.log("Object.is(negzero, -0) => ", Object.is(negzero, -0)); // true
console.log("Object.is(negzero, 0) => ", Object.is(negzero, 0)); // false

//signed numbers can be used to indicate for example the velocity of the car
//where abs(x) would represent the speed and sign would represent the direction
//it could also be used to know the trend of the stock.. for e.g. if the stock price
//moves from Rs450 to Rs400 and comes back to 450, it would be represented as +0 to indicate that
//the stock price is in the upward direction
//however if the stock price moves from 450 to 475 and then moves down to 450, it could be represented as -0
//to indicate that the stock price is in the downward direction

console.log(Math.sign(-2)); // -1
console.log(Math.sign(+2)); // 1

console.log(Math.sign(-0)); // -0 returns -0 instead of -1
console.log(Math.sign(+0)); // 0  returns  0 instead of 1


