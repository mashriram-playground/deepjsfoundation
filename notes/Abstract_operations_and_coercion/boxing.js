let student = "shriram";
console.log(student.length); // how can a primitive type like string have the access to a method
// (this is when boxing happens) javascript recognises that you are accessing a method for somethings that is not an object
// hence it converts the primitive into an object implicitly
