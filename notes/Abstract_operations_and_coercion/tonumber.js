console.log(Number("")); // => 0 # the root of all evil!!
console.log(Number("0")); // => 0
console.log(Number("-0")); // => -0
console.log(Number(" 009   ")); // => 9
console.log(Number("3.1415")); // => 3.1415
console.log(Number("0.")); // => 0
console.log(Number(".0")); // => 0
console.log(Number(".0.1")); // => NaN
console.log(Number(".")); // => NaN
console.log(Number("0xaf")); // => 175
console.log(Number(false)); // => 0
console.log(Number(true)); // => 1
console.log(Number(null)); // => 0
console.log(Number(undefined)); // => NaN
// the numberification ie toNumber(object) will directly go ahead and do toString(object) since valueOf(object) is NaN
console.log(Number({ name: 2 })); // => NaN # and it will try to do a toNumber on the string that is returned from toString operation
console.log(Number([])); // => 0
console.log(Number([1, 2, 3])); // => NaN
console.log(Number([[]])); // => 0
console.log(
  Number({
    toString() {
      return "3";
    },
  })
); // => 3 # first it calls valueOf of object which is NaN so it proceeds to toString which gives "3"
//the "3" since a primitive is again passed in to valueOf which gives 3
