console.log(String(-0)); // => "0" the edge case
console.log(String(null)); // => "null"
console.log(String(undefined)); // => "undefined"
console.log(String(true)); // => "true"
console.log(String(false)); // => "false"
console.log(String(3.1456)); // => "3.1456"
console.log(String(0)); // => "0"

// ToString(object) => calls => ToPrimitive(string) => toString() first ,toString behaves like String()
console.log(String([])); // => ""
console.log(String([1, 2, 3])); // => "1,2,3"
console.log(String([null, undefined])); // => ","
console.log(String([[[], [], []], [], []])); // => ",,,,"
console.log(String([, , , ,])); // => ",,," # notice that one comma is missing in the output, that is because the empty array gets resolved as below
console.log(String([null, null, null, null])); // => ",,," # the last comma  at the end of the array is dropped
console.log(String({})); // => [object Object]
console.log(
  String({
    toString() {
      return "I am an Object";
    },
  })
);
