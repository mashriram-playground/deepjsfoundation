console.log(1 == 1); // => true # double equals performs a triple equals when the types match no matter what the type is
console.log(1 === 1); // => true
console.log(1 == "1"); // => true # The double equals to operator coerces the values when two different types are involved and it generally prefers the ToNuumber() operation hence here converts the "1"=> 1
console.log(1 === "1"); // => false
console.log("" == 0); //  => true #The double equals to operator , since it does ToNumber() the string is coerced to 0 OOPS! hence its true
console.log(undefined == undefined);
console.log(null == null);
console.log(undefined == null); // => true #undefined equals to null and these two can never be equal to any other values other than these cases
console.log([] == ![]); // => true # When the type dealt in either side is an object then => ToPrimitive() takes place to convert object to primitive
// The ToPrimitive([]) => "";
// Now the other side ![] is a boolean so it performs a lookup and since any object is truthy ![] is falsy => therefore the RHS is false
//now the LHS and RHS are of different types LHS = "", RHS = false
//hence both undergo ToNumber() which gives
//ToNumber("") => 0 , ToNumber(false) => 0
//Now it comes down to 0 == 0, So it undergoes the tripe equals operation since both sides have same types
//0===0; is true
//Hence the ouput is true
console.log([1] == 1); // => true # Due to ToPrimitive([1]) => '1' and ToNumber('1') => 1
console.log([] == true); // => false due to Toprimitive([]) => "" and ToNumber("") => 0 and ToNumber(true) => 1
console.log([1, 2, 3] == true); // => false due to Toprimitive([1,2,3]) => "NaN" and ToNumber("NaN") => NaN and ToNumber(true) => 1
